FROM node:9.2.0-alpine as front
RUN mkdir -p /app/front/uno/
WORKDIR /app/front/uno/
COPY ./app/static/front/package.json /app/front/uno/
RUN npm install
RUN npm i npm@latest
COPY ./app/static/front/ /app/front/uno/
RUN npm run-script build


FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3-pip python3.6 build-essential
RUN pip3 install pipenv
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
RUN mkdir -p /app/uno/
WORKDIR /app/uno/
COPY ./Pipfile /app/uno/
COPY ./Pipfile.lock /app/uno/
RUN pipenv install --system --deploy
COPY . /app/uno/
COPY --from=front /app/front/uno/ /app/uno/app/static/front/
EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["server.py"]
