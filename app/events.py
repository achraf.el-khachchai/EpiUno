import csv
from flask_socketio import SocketIO, emit, join_room, leave_room
from flask import request

from app.config import security
from app.utils.audio import audio_query, text_query
from app.utils.utils import unique_key
from app.utils.uno.card import Card
from app.utils.uno.game import ACTIONS


socketio = SocketIO()


def send_data(event_name, room=None, broadcast=False, **kwargs):
    if 'key' not in kwargs:
        kwargs['key'] = unique_key()
    emit(event_name, kwargs, room=room, broadcast=broadcast)


def send_message(type="message", broadcast=True, **kwargs):
    if not broadcast:
        send_data('chat receive', type=type, **kwargs)
    else:
        security.room_send_data(emit, 'chat receive', type=type, **kwargs)


def send_player_response(room=security.current_player, **kwargs):
    send_data("player response", type="warning", room=room, **kwargs)


@socketio.on('connect')
def connect():
    pass


@socketio.on('disconnect')
def disconnect():
    user = security.current_player
    print('deco req sid %s (%s)' % (user, request.sid))
    room = security.get_room()
    security.disconnect()
    if room:
        security.room_send_data(emit, 'room', players=room.users, game_started=room.game.started, owner=room.users[0])
        if room.game.over:
            security.room_send_data(emit, 'game over', winner=room.users[0])


@socketio.on('join')
def on_join(data):
    username = data['username']
    room = data['room']
    join_room(room)
    send_message(sender='server', msg=username + ' has entered the room.', type='join')
    print('USER %s JOINS %s' % (username, room))


@socketio.on('leave')
def on_leave(data):
    username = data['username']
    room = data['room']
    leave_room(room)
    send_message(sender='server', msg=username + ' has left the room.', type='leave')
    print('USER %s LEAVES %s' % (username, room))


@socketio.on('chat message')
def on_chat_msg(data):
    curr = security.current_player
    if curr is None:
        emit('logout')
        return
    print('CURR %s HAS SID %s WHEN REQ HAS %s' % (curr, security.get_sid(user=curr), request.sid))
    print(security.current_player, data)
    send_message(sender=security.current_player, msg=data)


@socketio.on('get-session')
def get_session():
    room = security.get_room()
    if room:
       room = room.name
    else:
        room = ""
    emit('get-session', {'user': security.current_player, 'room_name': room})


@socketio.on('request-login')
def request_login(data):
    username = data['user']
    if security.is_authenticated() and (username == security.current_player or not username):
        emit('validate-login', security.current_player)
    if not security.authenticate(username):
        emit('deny-login', 'This name is already used')
        return
    emit('validate-login', security.current_player)
    security.register_sid(request)
    print("connected: %s (%s)" % (security.current_player, security.get_sid()))
    emit('rooms', security.all_rooms)


@socketio.on('create-room')
def create_room(room):
    if not security.is_authenticated():
        emit('logout')
        return
    result = security.add_room(room)
    emit('create-room', result is not None)
    if result:
        emit('rooms', security.all_rooms, broadcast=True)
        security.room_send_data(emit, 'room', players=result.users, game_started=result.game.started, owner=result.users[0])


@socketio.on('leave-room')
def leave_room():
    room = security.get_room()
    if not security.is_authenticated() or not room:
        emit('logout')
        return
    security.leave_room()
    emit('rooms', security.all_rooms, broadcast=True)
    security.room_send_data(emit, 'room', room=room, players=room.users, game_started=room.game.started, owner=room.users[0])


@socketio.on('join-room')
def join_game_room(room_id):
    if not security.is_authenticated():
        emit('logout')
        return
    if security.join_room(room_id):
        room = security.get_room()
        emit('rooms', security.all_rooms, broadcast=True)
        security.room_send_data(emit, 'room', players=room.users, game_started=room.game.started, owner=room.users[0])
        send_message(sender="server", msg="Welcome to the %s play room !" % room.name, room=request.sid, broadcast=False)
    else:
        room = security.get_room_id(room_id)
        send_data('fail-join-room', room=request.sid, name=room.name, id=room.id, players=room, status=room.game.status)


@socketio.on('join-game')
def enter_game_room():
    room = security.get_room()
    if not security.is_authenticated() or not room:
        emit('logout')
        return

    security.room_send_data(emit, 'room', players=room.users, game_started=room.game.started, owner=room.users[0])


@socketio.on('start-game')
def on_game_start():
    room = security.get_room()
    if not security.is_authenticated() or not room:
        emit('logout')
        return
    room.game.start_game(room.users)
    emit('rooms', security.all_rooms, broadcast=True)
    print('STARTING GAME')
    security.room_send_data(emit, 'game-start')
    emit_states(room)


@socketio.on('audio-query')
def audio_url(audio):
    query = audio_query(audio).query_result
    print(query)
    handle_result(security.get_room(), security.current_player, query)


@socketio.on('text-query')
def text_url(text):
    query = text_query(text).query_result
    handle_result(security.get_room(), security.current_player, query, text)


def emit_states(room):
    security.room_send_data(emit, 'game state', room=room, **room.game.player_state)
    for player in room.users:
        emit('player state', room.game.player_hand(player), room=security.get_sid(player))
        print(player, room.game.player_hand(player))
    cards_state = [{'name': p, 'cards': len(room.game.player_hand(p)['hand'])} for p in room.users]
    security.room_send_data(emit, 'cards state', players=cards_state)


def handle_result(room, player, result, text=None):
    game = room.game
    action = result.action
    if action in ACTIONS:
        role, move = action.split('.')
        if game.can_play(player, role):
            if move in ('DRAW', 'END'):
                msg = game.play(player, move)
            else:
                card, color = None, None
                print(result.parameters)
                if move == 'CARD':
                    ctype = result.parameters.fields['card_type'].string_value
                    ccolor = result.parameters.fields['card_color'].string_value
                    if '' in (ctype, ccolor):
                        msg = "I don't understand the card you want to play."
                    else:
                        card = Card(ctype, ccolor)
                elif move == 'JOKER':
                    print(result.parameters)
                    joker = result.parameters.fields['joker'].string_value
                    ccolor = result.parameters.fields['card_color'].string_value
                    if '' in (joker, ccolor):
                        print(joker, ccolor)
                        msg = "I don't understand the joker and the color you want."
                    else:
                        card = Card(joker, 'black')
                        color = ccolor
                if card is not None:
                    msg = game.play(player, move, card, color)
        else:
            msg = "It's not your turn, don't rush it."
    else:
        print(result)
        with open('log.csv', 'a') as log:
            writer = csv.writer(log)
            writer.writerow((text, result))
        msg = "I didn't get what you just said."
    emit_states(room)
    print('Player Response %s' % msg)
    send_player_response(message=('{} ({})'.format(msg, result.query_text)))
    if game.over:
        security.room_send_data(emit, 'game over', winner=player)


@socketio.on('game over')
def game_over_details():
    room = security.get_room()
    if room and room.game.over:
        state = room.game.state
        send_data('game over details', room=request.sid, game=room.game.state)


@socketio.on('play again')
def play_again():
    room = security.get_room()
    if room:
        room.game.reset()
        security.room_send_data(emit, 'game restart', room)
        security.room_send_data(emit, 'logout', room)
    else:
        emit('logout')
