# !/usr/bin/env python
# coding: utf-8
import datetime

from flask import render_template, Blueprint, request, send_from_directory

from .utils import utils
from .config import security

app = Blueprint('main', __name__)


@app.context_processor
def context_preprocessor():
    return {'user': security.current_player,
            'now': datetime.datetime.now,
            'date': utils.format_date}


@app.route('/')
def app_home():
    return render_template('index.html')


@app.route('/fill', methods=['POST'])
def fill():
    print('FILL REQ %s' % request)


@app.route('/libs/<path:path>')
def serve_node_module(path):
    return send_from_directory("../node_modules", path)
