import dialogflow
from uuid import uuid4
from google.oauth2 import service_account
from dialogflow_v2 import SessionsClient, enums
from dialogflow_v2.types import InputAudioConfig, QueryInput, QueryParameters

credz = service_account.Credentials.from_service_account_info({
        "type": "service_account",
        "project_id": "nothing-1-38506",
        "private_key_id": "f06a7697f15bad68ec5a1564fe7bbaf6824637de",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDq7oP82C/ehX33\neKYpzh7Neb7CkVTaWoWwzE0DouS3fVQh6ujzgCEB0TdT9Pg2w2M+Amfk7wbMesq+\nnMbUW7KZkX9q2w1uWToRqEWE4cijyBNw0P/Etl6dm2nxq2JU/WaBQ4A9XYI9ZYNN\n5iorJuIO+Nx0eDLxv9N+5e7v27QW8tsvJ+ZQZzdTqrIo8lB8/a4dblt2XsPN7kNA\nrNdfB13hTcdQ5/GPH6dcDayLRqyGYD/JGyVwadxgLYsJOHw4j+o1NggmNBdbs2Jb\nukn/e/b9cc/E3qJLvgSxm0D/DDwaTanfHLOlmh/VbjqseWeQAWgpXnLDbYV8zYc8\nV+MIB7qhAgMBAAECggEAAaTXGQWZfw0htKg9x+qhzl8ZWuhpAfqxDavK554L8gqz\nLdlUWKTzPi1mZBmEsN8cEAf7GoGC03r+u94yMzqIyN1JQvORUyeWG4X/f6Ktku1R\nPjU2yMI14Wes1ZIrbT2YcKo+CnF8gHUObRgU2chTIibcrCbQ6vGERfXPKFmKB7pq\nZ6qqftQ+beZ3qsa9rC2edRx1lwhnw8g0ZkL9Z4NKeH2KcoqvNtHJEWaw0tAdGgV2\n/hw/973rEpHlZRlod+QMbocu+HQ/HJkdwGL0JNs80AMxd5L3TX2GwXRZ1hV9JwIy\n8n5t4qIgdZnY9Nm63eOv5wL8q20Ra5da8VX/vfhO8QKBgQD9tH1/TSVKST0diHCR\nvXUderGpchN3PAsO6zQ7q9ljivxuXQsnzjVvubpODTupLN7vZe1s51CTfR8R0V4O\nMy6DcCthws3PH/MYkqdIO1ElB7m2vDoe9wrlrkiSSF4WoX3Kf+Wlzx/bJL48YooX\nIyLjoS35Jth1V83vcqXjqgbG8QKBgQDtDo0zLSGV4B9gQQQfKKdsKLmqk19j09ND\nXoN2w52Fybg67BlhnpVqEjuvqGY8EVnWxMbApDyiSb2ZCINeu6KTrADRndu2U9Zd\n3c9PjQKKMte9gFZ8DmiVvU89kxmAtcN7wjs3stBExbqokNoyPB59OENMS+rGd1aB\nWECgB+cOsQKBgQDv7HVWIwWl19cVC8ew+6M6qqJC5R+lfcHTbPjriVu++Fw6noED\nNs5wBws57ufB2C5H5XN/rs0TfZsZRY8WDK7rG/PastfhDYzeayoJdBcL1t95AU5R\npDzI8mWs+QUJj9Ue4I6znEm6+2dwQZvnv5GqBO4uFd9UMSIOVLSutUdNcQKBgBKv\nqPW1wwX7ar29okOKgtgFxczqXQSTd/3Bf39nIA2hvwH2BnIpfAGmekn4bkah34EM\nddZNm/H2+hkMEL7w73nQfygG14Dhzu4LRAIzTVXzeAoaCbcxs5DKIWrrI4F/0m74\n+o4DbGNfar1K3chD8OtR+gwTl+gZFBqSCFYVCWOhAoGASpN+vAv1luJCAz905MhT\n3BbxlFVwHcT8i6AuIptlcZEWfsJMcOubJ3LndHld/7OR9XKKBuR+O98Ns2+2H7n2\nQaltk8Uks/cVu716WGxEBV6aY6ropKzHhk5Y7hJ2q67EF3H79jAMkWSOdLLaNlfD\n6F6SmsMWXgiExEvI1/kI5/c=\n-----END PRIVATE KEY-----\n",
        "client_email": "nothing@nothing-1-38506.iam.gserviceaccount.com",
        "client_id": "109232892002307022196",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://accounts.google.com/o/oauth2/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/nothing%40nothing-1-38506.iam.gserviceaccount.com"
})
client = SessionsClient(credentials=credz)
session = client.session_path('nothing-1-38506', str(uuid4()))
parameters = QueryParameters(reset_contexts=True)


def audio_query(audio, language_code='en'):
    print(len(audio))
    config = InputAudioConfig(
        audio_encoding=enums.AudioEncoding.AUDIO_ENCODING_LINEAR_16,
        language_code=language_code,
    )
    query = QueryInput(audio_config=config)
    return client.detect_intent(
            session=session, query_input=query, input_audio=audio, query_params=parameters)


def text_query(text, language_code='en'):
    text_input = dialogflow.types.TextInput(text=text, language_code=language_code)
    query_input = dialogflow.types.QueryInput(text=text_input)
    return client.detect_intent(
            session=session, query_input=query_input, query_params=parameters)
