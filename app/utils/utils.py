import time
import datetime


def unique_key():
    return time.time() * 1000000


def format_date(d: datetime.datetime, fmt: str):
    return d.strftime(fmt)
