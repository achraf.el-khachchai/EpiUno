from flask import session

from app.utils.uno.game import Game


class SecurityContext:
    def __init__(self):
        self.users = list()
        self.sids = dict()
        self.rooms = list()
        self.room_id = 1

    def _add_user(self, user, keep_sid=False, **args):
        if user not in self.users:
            self.users.append(user)
            if not keep_sid:
                self.sids[user] = None

    def _remove_user(self, user, keep_sid=False, **args):
        self.users.remove(user)
        if not keep_sid:
            del self.sids[user]

    def authenticate(self, user, **args):
        if self.is_authenticated(**args):
            if session['username'] != user:
                if session['username'] in self.users:
                    self._remove_user(session['username'], **args)
            else:
                self._add_user(user, **args)
                return True
        if user in self.users:
            return False
        session['username'] = user
        self._add_user(user, **args)
        return True

    def disconnect(self, user=None, **args):
        if user is None:
            user = self.current_player
        if user in self.users:
            self.leave_room()
            self._remove_user(user, **args)

    def register_sid(self, request):
        user = self.current_player
        if not self.is_authenticated(user=user):
            self._add_user(user)
        self.sids[user] = request.sid

    def get_sid(self, user=None):
        if user:
            return self.sids[user]
        return self.sids[self.current_player]

    def is_authenticated(self, user=None, **args):
        if user:
            return user in self.users
        try:
            return 'username' in session
        except RuntimeError:
            return None

    @property
    def all_rooms(self):
        return [{'id': r.id, 'name': r.name, 'players': len(r.users), 'status': r.game.status} for r in self.rooms]

    @property
    def players(self):
        return self.users

    @property
    def current_player(self):
        if not self.is_authenticated():
            return None
        if not self.is_authenticated(session['username']):
            self._add_user(session['username'])
        return session['username']

    @property
    def current_room(self):
        return self.get_room()

    def other_players(self):
        if not self.is_authenticated():
            return self.players
        index = self.players.index(session['username'])
        return self.players[:index] + self.players[index + 1:]

    def change_name(self, name, **args):
        self._remove_user(self.current_player, keep_sid=True, **args)
        self._add_user(name, keep_sid=True, **args)

    def get_room(self, room_name=None, player=None):
        room = []
        if room_name is None and player is None:
            player = self.current_player
        if room_name is not None:
            room = [x for x in self.rooms if x.name == room_name]
        elif player is not None:
            room = [x for x in self.rooms if player in x.users]

        if not len(room):
            return None
        return room[0]

    def get_room_id(self, room_id):
        room = [x for x in self.rooms if x.id == room_id]
        if not len(room):
            return None
        return room[0]

    def add_room(self, room_name):
        print(room_name)
        print(self.rooms)
        if self.get_room(room_name):
            return False
        room = Room(room_name, self.room_id)
        self.room_id += 1
        self.rooms.append(room)
        self.join_room(room.id)

        return room

    def join_room(self, index):
        player = self.current_player
        if not player or self.get_room(player=player):
            return False
        room = self.get_room_id(index)

        room.join(player)
        return True

    def leave_room(self, room_name=None, player=None):
        if room_name is not None:
            room = self.get_room(room_name)
        else:
            room = self.get_room(player=player)

        if not room:
            return False

        room.leave(player if player else self.current_player)
        if not room.users:
            self.rooms.remove(room)
        return True

    def room_send_data(self, emit, event_name, room=None, **kwargs):
        if room is None:
            room = self.get_room()
        if not room:
            return
        kwargs['room_name'] = room.name
        for player in room.users:
            emit(event_name, kwargs, room=self.get_sid(player))

    def is_room_owner(self, player=None, room=None):
        if player is None:
            player = self.current_player
        if room is None:
            room = self.get_room()
        if not player or not room:
            return False
        return player == room.owner


class Room:
    def __init__(self, name, id):
        self.name = name
        self.users = list()
        self.game = Game()
        self.id = id

    @property
    def owner(self):
        return self.users[0]

    def join(self, user):
        if self.game.started:
            return
        self.users.append(user)

    def leave(self, user):
        if user not in self.users:
            return
        self.users.remove(user)
        self.game.remove_player(user)
