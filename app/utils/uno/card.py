"""Uno cards contants & Card class"""

COLORS = ["red", "green", "yellow", "blue"]
SPES = ['+2', 'Reverse', 'Skip']
NUMS = [str(i) for i in range(10)]
TYPES = [*NUMS, *SPES]
JOKERS = ['Wildcard', '+4']


class Card:
    """Uno card"""
    def __init__(self, card_type, color, static_url="/static/img/card", **args):
        self.type = card_type
        self.color = color
        self.static_url = static_url
        for arg in args.keys():
            setattr(self, arg, args[arg])

    def __str__(self):
        return "Card {} {}".format(self.type, self.color)

    def __eq__(self, other):
        if isinstance(other, Card):
            return self.type == other.type and self.color == other.color
        return False

    @property
    def image(self):
        return "{}/{}_{}.png".format(self.static_url,
                                     str(self.color).lower(),
                                     str(self.type).lower())

    def valid_start(self):
        return self.type.isdigit()

    def valid_play(self, card, game_color=None, cut=False):
        if cut:
            return self.type == card.type and self.color == card.color
        return (
            card.color == 'black' or
            self.type == card.type or
            self.color == card.color or
            (self.color == 'black' and card.color == game_color))

    def json(self):
        return {'type': self.type, 'color': self.color, 'image': self.image}
