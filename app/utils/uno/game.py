"""Uno game flow"""

from random import shuffle

from .card import Card, COLORS, TYPES, JOKERS

ACTIONS = ['CURR.DRAW', 'CURR.END', 'ANY.CARD', 'ANY.JOKER']
#TODO: CURR.UNO, ANY.COUNTER


class Game(object):

    def __init__(self):
        self.started = False
        self.over = False
        self.deck = list()
        self.stack = list()
        self.players = list()
        self.hands = dict()
        self.current = 0
        self.orientation = 1
        self.curr_drew = False
        self.color = ''

    def _initialize(self, **kwargs):
        self.deck = list()
        for color in COLORS:
            for card_type in TYPES:
                self.deck.append(Card(card_type, color, **kwargs))
                # Add 2 of each except for 0.
                if not card_type == '0':
                    self.deck.append(Card(card_type, color, **kwargs))
        for _ in range(0, 4):
            for joker in JOKERS:
                self.deck.append(Card(joker, 'black', **kwargs))

        shuffle(self.deck)

    def reset(self):
        self.started = False
        self.over = False
        self.deck = list()
        self.stack = list()
        self.players = list()
        self.hands = dict()
        self.current = 0
        self.orientation = 1
        self.curr_drew = False
        self.color = ''

    def start_game(self, players):
        self.started = True
        self.over = False
        self._initialize()
        self.stack = list()
        self.players = players
        self.hands = {
            player: [self.deck.pop() for _ in range(7)]
            for player in players
        }
        self.current = 0
        self.orientation = 1
        self.curr_drew = False
        while len(self.stack) == 0:
            card = self.deck.pop()
            if card.valid_start():
                self.stack.append(card)
            else:
                self.deck.append(card)
                shuffle(self.deck)
        self.color = self.stack[0].color

    @property
    def current_player(self):
        return self.players[self.current]

    @property
    def status(self):
        status = 'waiting'
        if self.started and not self.over:
            status = 'playing'
        if self.over:
            status = 'ended'
        return status

    @property
    def state(self):
        return {'players': self.players_status,
                'status': self.status}

    @property
    def players_status(self):
        return [
            {'player': p, 'cards': [c.json() for c in self.hands[p]]}
            for p in self.players
        ]

    @property
    def player_state(self):
        card = self.stack[-1].json()
        player = self.players[self.current]
        return {'card': card, 'player': player, 'color': self.color}

    def next_player_index(self, player):
        current = self.players.index(player) + self.orientation
        return current % len(self.players)

    def player_draw(self, player, draws):
        for _ in range(draws):
            if len(self.deck) == 0:
                shuffle(self.stack)
                self.deck, self.stack = self.stack, []
            draw = self.deck.pop()
            self.hands[player].append(draw)

    def can_play(self, player, role):
        if player in self.players:
            curr = player == self.current_player
            return role == 'ANY' or (role == 'CURR' and curr)
        return False

    def play(self, player, move, card=None, color=None):
        return ACTIONS_DICT[move](self, player, card, color)

    def draw_action(self, player, card, color):
        if self.curr_drew:
            return 'You already drew a card this turn.'
        self.curr_drew = True
        self.player_draw(player, 1)
        return 'You drew a card, you can play ir or end your turn.'

    def end_action(self, player, card, color):
        if not self.curr_drew:
            return "You can't end your turn until you play or draw a card."
        return self.end_turn('Your turn is over.')

    def card_action(self, player, card, color):
        if card not in self.hands[player]:
            return "You can't play a card that's not in your hand."

        if self.stack[-1].valid_play(card, self.color, player != self.current_player):
            joker = card.color == 'black'
            self.hands[player].remove(card)
            self.stack.append(card)
            self.current = self.players.index(player)
            if len(self.hands[player]) == 0:
                if joker:
                    msg = "You can't win the game with a joker card."
                    self.player_draw(player, 2)
                else:
                    self.over = True
                    return 'You win !'
            else:
                if card.type in ('+2', '+4'):
                    draws = int(card.type[1])
                    next_player = self.players[self.next_player_index(player)]
                    self.player_draw(next_player, draws)
                if card.type == 'Reverse':
                    self.orientation *= -1
                if card.type in ('+2', 'Skip'):
                    self.current = self.next_player_index(player)
                self.color = color if card.color == 'black' else card.color
                msg = "Now that you played a card your turn is over."
        else:
            self.player_draw(player, 2)
            msg = "You tried to make an invalid play, that's two cards for you."
            if player != self.current_player:
                return msg

        return self.end_turn(msg)

    def end_turn(self, msg):
        self.curr_drew = False
        self.current = self.next_player_index(self.current_player)
        return msg

    def player_hand(self, player):
        return {'hand': [card.json() for card in self.hands[player]]}

    def remove_player(self, player):
        if player not in self.players:
            return
        if self.current_player == player:
            self.current = self.next_player_index(player)
        self.players.remove(player)
        del self.hands[player]
        if len(self.players) == 1:
            self.over = True


ACTIONS_DICT = {
    'DRAW': Game.draw_action,
    'END': Game.end_action,
    'CARD': Game.card_action,
    'JOKER': Game.card_action,
}
