import os, string, random

from app.utils.security import SecurityContext
from app.utils.uno.game import Game

basedir = os.path.abspath(os.path.dirname(__file__))

security = SecurityContext()


class Config(object):
    SECRET_KEY = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))


class DevelopmentConfig(Config):
    FILEDIR = basedir + '/static/files/'
