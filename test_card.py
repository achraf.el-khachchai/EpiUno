#!/usr/bin/env python3

import random
import unittest

from app.utils.uno.card import Card, COLORS, TYPES, JOKERS, NUMS, SPES
from app.utils.uno.game import Game


class TestCards(unittest.TestCase):

    def test_start(self):
        for color in COLORS:
            for num in NUMS:
                card = Card(num, color)
                msg = '{} must be True on valid_start'.format(card)
                self.assertEqual(card.valid_start(), True, msg)
            for spe in SPES:
                card = Card(spe, color)
                msg = '{} must be False on valid_start'.format(card)
                self.assertEqual(card.valid_start(), False, msg)
        for joker in JOKERS:
            card = Card(joker, 'black')
            msg = '{} must be False on valid_start'.format(card)
            self.assertEqual(card.valid_start(), False, msg)

    def test_color(self):
        random.shuffle(COLORS)
        c0, c1 = Card('0', COLORS[0]), Card('+2', COLORS[0])
        msg = '{}.valid_play({}) must be True'.format(c0, c1)
        self.assertEqual(c0.valid_play(c1), True, msg)
        joker = Card('+4', 'black')
        msg = '{}.valid_play({}, {}) must be True'.format(joker, c1, COLORS[0])
        self.assertEqual(joker.valid_play(c1, COLORS[0]), True, msg)

    def test_type(self):
        random.shuffle(TYPES)
        c0, c1 = Card(TYPES[0], 'red'), Card(TYPES[0], 'blue')
        msg = '{}.valid_play({}) must be True'.format(c0, c1)
        self.assertEqual(c0.valid_play(c1), True, msg)

    def test_joker(self):
        joker = Card('Wildcard', 'black')
        for color in COLORS:
            for card_type in TYPES:
                card = Card(card_type, color)
                msg = '{}.valid_play({})'.format(card, joker)
                self.assertEqual(card.valid_play(joker), True, msg)
        for jok_type in JOKERS:
            jok_card = Card(jok_type, 'black')
            msg = '{}.valid_play({}, red) must be True'.format(jok_card, joker)
            self.assertEqual(jok_card.valid_play(joker, 'red'), True, msg)
        c0, c1 = Card(TYPES[0], 'red'), Card(TYPES[0], 'blue')
        msg = '{}.valid_play({}, red) must be True'.format(joker, c0)
        self.assertEqual(joker.valid_play(c0, 'red'), True, msg)
        msg = '{}.valid_play({}, red) must be False'.format(joker, c1)
        self.assertEqual(joker.valid_play(c1, 'red'), False, msg)


class TestGame(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.game = Game()

    def assert_game(self, started, over, dlen, slen, plen, curr, orientation,
        drew, color=None, hands=None, lhands=None):
        self.assertEqual(started, self.game.started)
        self.assertEqual(over, self.game.over)
        self.assertEqual(dlen, len(self.game.deck))
        self.assertEqual(slen, len(self.game.stack))
        self.assertEqual(plen, len(self.game.players))
        self.assertEqual(curr, self.game.current)
        self.assertEqual(orientation, self.game.orientation)
        self.assertEqual(drew, self.game.curr_drew)
        if color is not None:
            self.assertEqual(color, self.game.color)
        if hands is not None:
            self.assertDictEqual(hands, self.game.hands)
        if lhands is not None:
            for key, val in lhands.items():
                self.assertEqual(val, len(self.game.hands[key]))

    def test_start(self):
        self.game.start_game(['foo', 'bar'])
        color = self.game.stack[-1].color
        self.assert_game(True, False, 93, 1, 2, 0, 1, False, color)

    def test_any(self):
        self.game.start_game(['foo', 'bar'])
        self.assertEqual(self.game.can_play('bar', 'ANY'), True)
        self.assertEqual(self.game.can_play('foo', 'ANY'), True)

    def test_curr(self):
        self.game.start_game(['foo', 'bar'])
        self.assertEqual(self.game.can_play('bar', 'CURR'), False)
        self.assertEqual(self.game.can_play('foo', 'CURR'), True)

    def test_draw(self):
        self.game.start_game(['foo', 'bar'])
        self.game.draw_action('foo', None, None)
        self.assert_game(True, False, 92, 1, 2, 0, 1, True)

    def test_end(self):
        self.game.start_game(['foo', 'bar'])
        self.game.end_action('foo', None, None)
        self.assert_game(True, False, 93, 1, 2, 0, 1, False)
        self.game.draw_action('foo', None, None)
        self.assert_game(True, False, 92, 1, 2, 0, 1, True)
        self.game.end_action('foo', None, None)
        self.assert_game(True, False, 92, 1, 2, 1, 1, False)

    def test_win(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [Card('2', 'red')]
        self.game.stack = [Card('2', 'red')]
        self.game.card_action('foo', Card('2', 'red'), None)
        self.assert_game(True, True, 93, 2, 2, 0, 1, False)

    def test_unowned_card(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [Card('1', 'red'), Card('1', 'blue')]
        self.game.hands['bar'] = [Card('2', 'red'), Card('3', 'blue')]
        self.game.stack = [Card('2', 'red')]
        self.game.card_action('foo', Card('3', 'red'), None)
        self.assert_game(True, False, 93, 1, 2, 0, 1, False)
        self.game.card_action('bar', Card('3', 'red'), None)
        self.assert_game(True, False, 93, 1, 2, 0, 1, False)

    def test_valid_play_cut_num(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [Card('1', 'red'), Card('1', 'blue')]
        self.game.hands['bar'] = [Card('2', 'red'), Card('3', 'blue')]
        self.game.stack = [Card('2', 'red')]
        self.game.card_action('bar', Card('2', 'red'), None)
        lhands = {'bar': 1, 'foo': 2}
        self.assert_game(True, False, 93, 2, 2, 0, 1, False, lhands=lhands)
        self.game.card_action('foo', Card('1', 'red'), None)
        lhands = {'bar': 1, 'foo': 1}
        self.assert_game(True, False, 93, 3, 2, 1, 1, False, lhands=lhands)

    def test_invalid_play_cut_num(self):
        self.game.start_game(['foo', 'bar', 'tmp'])
        self.game.hands['foo'] = [Card('1', 'red'), Card('1', 'blue')]
        self.game.hands['bar'] = [Card('2', 'red'), Card('3', 'red')]
        self.game.stack = [Card('2', 'red')]
        self.game.card_action('bar', Card('3', 'red'), None)
        lhands = {'bar': 4, 'foo': 2, 'tmp': 7}
        self.assert_game(True, False, 84, 1, 3, 0, 1, False, lhands=lhands)
        self.game.card_action('foo', Card('1', 'blue'), None)
        lhands = {'bar': 4, 'foo': 4, 'tmp': 7}
        self.assert_game(True, False, 82, 1, 3, 1, 1, False, lhands=lhands)

    def test_draw2_2players(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [
            Card('+2', 'red'), Card('Reverse', 'red'), Card('Skip', 'red')
        ]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('+2', 'red'), None)
        lhands = {'bar': 9, 'foo': 2}
        self.assert_game(True, False, 91, 2, 2, 0, 1, False, lhands=lhands)

    def test_skip_2players(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [
            Card('+2', 'red'), Card('Reverse', 'red'), Card('Skip', 'red')
        ]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('Skip', 'red'), None)
        lhands = {'bar': 7, 'foo': 2}
        self.assert_game(True, False, 93, 2, 2, 0, 1, False, lhands=lhands)

    def test_reverse_2players(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [
            Card('+2', 'red'), Card('Reverse', 'red'), Card('Skip', 'red')
        ]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('Reverse', 'red'), None)
        lhands = {'bar': 7, 'foo': 2}
        self.assert_game(True, False, 93, 2, 2, 1, -1, False, lhands=lhands)

    def test_draw2_3players(self):
        self.game.start_game(['foo', 'bar', 'tmp'])
        self.game.hands['foo'] = [
            Card('+2', 'red'), Card('Reverse', 'red'), Card('Skip', 'red')
        ]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('+2', 'red'), None)
        lhands = {'bar': 9, 'foo': 2, 'tmp': 7}
        self.assert_game(True, False, 84, 2, 3, 2, 1, False, lhands=lhands)

    def test_skip_3players(self):
        self.game.start_game(['foo', 'bar', 'tmp'])
        self.game.hands['foo'] = [
            Card('+2', 'red'), Card('Reverse', 'red'), Card('Skip', 'red')
        ]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('Skip', 'red'), None)
        lhands = {'bar': 7, 'foo': 2, 'tmp': 7}
        self.assert_game(True, False, 86, 2, 3, 2, 1, False, lhands=lhands)

    def test_reverse_3players(self):
        self.game.start_game(['foo', 'bar', 'tmp'])
        self.game.hands['foo'] = [
            Card('+2', 'red'), Card('Reverse', 'red'), Card('Skip', 'red')
        ]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('Reverse', 'red'), None)
        lhands = {'bar': 7, 'foo': 2, 'tmp': 7}
        self.assert_game(True, False, 86, 2, 3, 2, -1, False, lhands=lhands)

    def test_draw4(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [Card('+4', 'black'), Card('Wildcard', 'black')]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('+4', 'black'), 'blue')
        lhands = {'bar': 11, 'foo': 1}
        self.assert_game(
            True, False, 89, 2, 2, 1, 1, False, lhands=lhands, color='blue')

    def test_wildcard(self):
        self.game.start_game(['foo', 'bar'])
        self.game.hands['foo'] = [Card('+4', 'black'), Card('Wildcard', 'black')]
        self.game.hands['bar'] = [Card('7', 'green'), Card('0', 'green')]
        self.game.stack = [Card('7', 'red')]
        self.game.card_action('foo', Card('Wildcard', 'black'), 'green')
        lhands = {'bar': 2, 'foo': 1}
        self.assert_game(
            True, False, 93, 2, 2, 1, 1, False, lhands=lhands, color='green')
        self.game.card_action('bar', Card('7', 'green'), None)
        lhands = {'bar': 1, 'foo': 1}
        self.assert_game(
            True, False, 93, 3, 2, 0, 1, False, lhands=lhands, color='green')


if __name__ == '__main__':
    unittest.main()
