INSTALL
=======

Python version required: 3.5+

First, install `pipenv` with `pip install pipenv`

Then, install project requirements with:
`pipenv install && npm install`

run the project with: `python server.py`
